# Teilnahme an Protesten

Wir haben eine Kurzanleitung entworfen, die als nützlicher Ratgeber zum selbst drucken, falten und in der Tasche mitführen geeignet ist. (PDF Herunterladen)

Heute mehr denn je müssen Bürger:innen in der Lage sein, durch Protestaktionen Machthaber:innen zur Rechenschaft zu ziehen und andere zu inspirieren.

Deine elektronischen Geräte und digitalen Besitztümer vor, während und nach dem Protest zu schützen ist essentiell um sowohl Dich und Deine Daten zu schützen als auch Deine Botschaft nach außen zu tragen.
Diebstahl, Beschädigung, Beschlagnahme, oder erzwungenes Löschen von Medieninhalten kann die Möglichkeit Deine Erlebnisse zu Veröffentlichen stören. Ebenfalls könnten diejenigen die am Protest teilnehmen Durchsuchungen und Verhaftungen ausgesetzt werden, und ihre Bewegungsprofile und Kontaktlisten könnten erfasst werden. Sie könnten Opfer von Überwachung und Unterdrückung werden.

Einem Protest beizuwohnen ist mit Risiken verbunden, und Maßnahmen zu ergreifen um diese gering zu halten können Dich - und Deine wertvollen Daten - absichern. Dieser Ratgeber gibt eine Übersicht über Schritte die du ergreifen kannst um vor, während und nach einem Protest deine Wirksamkeit zu maximieren und Dich und Deine Daten sicher zu halten.

Denke daran, das diese Tipps allgemeine Hinweise zum verbesserten Schutz Deiner Daten sind und keine Rechtsberatung darstellen. Wenn Du spezifische rechtliche Fragen hast, suche eine:n Rechtsberater:in auf.

## Vor dem Protest
### Verschlüssele den gesamten Speicher Deines Geräts

Vollverschlüsselung, also verschlüsselung des gesamten Speicherinhalts ("Full-disk encryption", "Telefon verschlüsseln") stellt sicher das alle Inhalte auf Deinem Gerät verschlüsselt sind. Diese Verschlüsselungstechnik schützt Daten, die auf dem Gerät selbst gespeichert sind, und ist nicht zu verwechseln mit Transportverschlüsselung für Daten die über das Internet übertragen werden. Sie kann helfen alles von auf dem Gerät gespeicherten Textnachrichten bis zu Passwörtern deines Browsers zu schützen. Wenn Dein Gerät von der Polizei beschlagnahmt, verloren, oder gestohlen wird, kann Vollverschlüsselung helfen die auf deinem Gerät gespeicherten Daten zu schützen. Die Lage in einem Protest ist oft unübersichtlich, es ist also durchaus denkbar, dass Du Dein Gerät verlieren könntest.

Android and iOS have long required full-disk encryption capabilities to be built into devices. These should be protected by a strong password : 8-12 random characters that are easy to remember and type in when you unlock your device. If devices are not protected by a strong password, the encryption may be easier to break using a brute-force attack. The iPhone 5s and later have specialized hardware to protect against this type of attack , but a complex, strong password is still the best practice.

It is important to note that encrypting your device will likely not encrypt external storage media such as SD or flash memory cards. You have to encrypt these separately, and may not be able to encrypt them at all. You might want to investigate where files are stored on your device using a file browsing app, or remove external storage media from your device altogether.

In addition, many digital cameras lack the ability to encrypt. It is safe to assume that photos and videos taken with digital cameras will be stored unencrypted, unless explicitly stated otherwise.
Remove fingerprint unlock and FaceID anchor link

Today, both iOS and Android allow users to unlock (and decrypt ) their devices with their fingerprint , and the iPhone X’s FaceID allows users to do the same with facial recognition. While these settings may seem appealing as convenient ways to enjoy the benefits of full-disk encryption, enabling them means an officer could physically force you to unlock your device with your fingerprint or face. In protest situations in particular—or in any other situation in which you may be more likely to encounter a government demand to search your phone (such as at a border crossing)—we suggest you turn this functionality off.

In the U.S., using a biometric—like your face scan or fingerprint—to unlock your phone may compromise protections for the contents of your phone afforded to you under the Fifth Amendment privilege against compelled incrimination. A police officer may try to intimidate you into “consenting” to unlock your phone, whether you use a biometric or a memorized passcode. But if you exercise your right to refuse and biometric unlocking functionality is turned on, an officer may physically force you to biometrically unlock your device. Under current U.S. law—which is still in flux—using a memorized passcode generally provides a stronger legal footing to push back in court against compelled device unlocking/decryption. While EFF continues to fight against attempts by law enforcement to compel people to decrypt their devices, there is currently less protection against compelled face and fingerprint unlocking than there is against compelled password disclosure.

    In iOS, you can disable this by going into Settings -> Touch ID & Passcode (or Settings -> Face ID & Passcode, depending on your iPhone version) and removing each of the fingerprints, or tapping Reset Face ID in this menu.
    In Android, disabling this feature may depend on your device manufacturer. For Pixel devices, go into Settings -> Security -> Pixel Imprint and delete the fingerprints from this menu.

Install Signal anchor link

Signal is an app available on both iOS and Android that offers strong encryption to protect both text messages and voice calls. This type of protection is called end-to-end encryption , which secures your communications in transit.

In addition to encrypting one-to-one communication, Signal enables encrypted group chats. The app also recently added the functionality of having messages disappear anywhere from 10 seconds to a week after they are first read. In contrast to some other services like SnapChat, these ephemeral messages will never be stored on any server, and are removed from your device after disappearing.

In 2016, a grand jury in the Eastern District of Virginia issued a subpoena to Open Whisper Systems, the developers of Signal. Because of the architecture of Signal, which limits the user metadata stored on the company’s servers, the only data they were able to provide was "the date and time a user registered with Signal and the last date of a user's connectivity to the Signal service."

In the context of a protest, you might send relevant photos and videos to friends who are remote using Signal, so that if your phone is confiscated you have a way to retrieve the media later.
Back up your data anchor link

Take precautions to limit the potential costs of losing access to your device, whether it’s lost, stolen or confiscated by law enforcement. Back up your data regularly and store that backup in a safe place to save yourself from a headache later on.
Buy a prepaid, disposable phone anchor link

In the United States, federal regulation does not require you to show your ID to purchase a prepaid SIM card (but your state might). Most countries require you to provide a form of ID to purchase a prepaid SIM card, thus linking the card to your identity and removing the possibility of anonymity.

If you're concerned about protecting the data stored on your device, don't bring it to the protest. Instead, purchase a prepaid mobile phone. These devices can be purchased along with a SIM card at most large retail stores. Let your friends know your temporary number, and use this to coordinate activities. Remember that the location of mobile devices can be determined by the cell towers they connect to. So, if you don't want your identity and location known, turn off your prepaid device before going home or anywhere that might reveal your identity. Using GPS should be safe, since GPS is a receiver and does not transmit any information. However, keep in mind that your device may store your coordinates. For this reason, we suggest you turn off location services before you travel to the protest, and leave it off until you return. Turning off location services may make it difficult to navigate if you are not familiar with the route to and from the protest. You may want to download a local copy of the area map you will be traveling in.

When you're done with the phone, it can be safely recycled or discarded from a location that is not linked to you. Keep in mind that if you carry both your regular device and a prepaid one with you, the location of these devices can be correlated and compromise your anonymity.
Dressing for anonymity and safety anchor link

Many law enforcement agencies have access to sophisticated surveillance technology that can be used to identify people attending a protest. To protect yourself, it’s important to dress in ways that preserve your anonymity and protect your physical safety.
Wear nondescript clothing

Wearing the same clothing as everyone in your group can help hide your identity during the protest and keep you from being identified and tracked afterwards. This not only helps preserve your anonymity, but also protects your fellow protestors from being identified and surveilled as well. Dressing in dark, monochrome colors will help you blend into a crowd. Be aware that you may not be as visible to cars in the dark, and should take extra precaution when crossing streets or walking near moving vehicles.

If you are able, bring a change of clothes for after the protest is over. You may be exposed to irritants such as pepper spray or tear gas during the protest, and it’s wise to change into clothing that won’t cause discomfort or harm on your return journey.
Cover identifying features

If you have visible tattoos or bright unconventional hair colors, cover them up. Tattoos can be used to identify you later, and may be added to databases for tattoo recognition. Dark monochrome hats, scarves, gloves, long sleeves, and full-length clothing will help cover these identifying features so you blend more easily into a crowd.
Wear protective equipment

Wear a mask! It will help protect you from airborne illnesses, face recognition technologies, and being identified in photos of the protest after it ends. Goggles are also useful for keeping your eyes safe from projectiles or irritants, and tinted ones may be useful for circumventing face recognition systems (but this strategy is not foolproof). Gloves will help protect your hands from irritants and contact with infectious diseases. If you are able to use medical gloves, use nitrile instead of latex gloves, as some people have latex allergies. Note that gloves will interfere with some phone functions. We also recommend wearing sturdy shoes that are comfortable to move around in for extended periods of time.
During the Protest anchor link
Take photos and videos without unlocking your device anchor link

Catching that perfect image is something you want to be ready for, and powerful images can help support your cause. If you've chosen a strong password, entering it into the device takes precious time, and you risk the moment passing before you're able to take the picture. Luckily, iOS and Android allow you to take photos and videos without unlocking your device.

    With Android Pixel devices, double-press the power button.
    At the iOS lock screen, you can firmly press on the camera icon. Older iOS devices require you to swipe.

Be mindful of other protestors in your photos and videos anchor link

If you are taking photos or videos of people at the protest, be mindful of what you post. If you post photos online where protestors or bystanders’ faces are identifiable, law enforcement or vigilantes might track them down and arrest or harass them. Consider obscuring the faces of anyone who has their eyes or mouth visible. You can edit photos in the default Android or iOS photo editing apps. Be sure to black out or blur other identifying features as well such as tattoos or unique clothing (blurring can sometimes be reversed so blacking out is better if you have the option). On Android, consider using the ObscuraCam app by The Guardian Project to edit photos, which will automatically blur faces. There is also a useful tool called Image Scrubber that can be used on any mobile device, including iOS or your PC.
Scrub metadata on photos anchor link

Once you are ready to post your photos, it’s a good idea to scrub the metadata contained in the image files if you don't want to leak personally identifying information. Metadata on photos can include information such as the model of camera the photo was taken on, the exact time and location where the photo was taken, and even your name.

    Option 1: Remove any original photo metadata by transferring the photo onto a desktop computer, taking a screenshot of the image, and posting the screenshot instead of the original photo.
    Option 2: You can also take a screenshot of the photo on your mobile device to remove the metadata, but the image quality may not be as high. You can then post that screenshot instead of the original photo.
    Option 3: Send yourself a copy of the photo via the Signal app (which strips metadata when sending images), then download the sent image for posting.

Things to be aware of while traveling to and from the protest anchor link
Driving considerations

Automated License Plate Reader Systems (ALPRs) automatically record the license plates of cars driving through an area, along with the exact time, date, and location they were encountered. This technology is often used by law enforcement in the United States and many other countries, or employed by private companies such as Vigilant and MVTrac who then share license plate data with law enforcement and other entities. Amassed in huge databases, this data is retained for lengthy periods of time. These companies have lobbied and litigated vigorously against statutes that would ban the private collection of license plate data or otherwise regulate ALPRs. Essentially, your location can be tracked over time based on the driving history of any car registered to you, with very few legal limits in place as to how this data can be collected, accessed, shared, and retained.

Read more in our Street Level Surveillance guide on ALPRs.
Public transit considerations

Be careful when traveling to and from the protest location. If you’re using payment methods or transit cards that are linked to you, law enforcement may be able to determine you attended the protest and track your movements. Consider using alternative means of public transportation if you would prefer that your movements and associations remain private.

If you are able, consider biking or walking to and from the protest to minimize exposure to these types of surveillance risks.
Enable airplane mode anchor link

Airplane mode ensures that your device will not be transmitting for the duration of your time at the protest, and prevents some (though possibly not all) of your location signals from being tracked. Unfortunately, this also means that you won't be able to message or call your friends, so plan accordingly. Before going to the protest, agree on a spot where you and your friends can meet if you get separated. As mentioned earlier in this guide, you may also want to turn off location services before you travel to the protest, and leave it off until you return.

Some apps allow you to navigate without having network access. Since GPS is a receive-only system, you can selectively turn GPS on after enabling airplane mode. Be sure to download a map of the area of the protest beforehand.
If you are arrested in the United States anchor link

If you are detained and questioned by police, you have a right to remain silent, and to speak with an attorney before and during any questioning. It is best to say “I want my attorney and I choose to remain silent” and then refuse to answer questions until you have a chance to talk to a lawyer.

If you answer questions, be sure to tell the truth. It is a crime to lie to a police officer and you may find yourself in more trouble for lying to law enforcement than for whatever it was they wanted to talk to you about in the first place.

If the police ask to see your phone, you can tell them that you do not consent to a search of your device. They might respond by seizing your phone and trying to search it later, but at least it will be clear that you did not give them permission to do so.

If the police ask for the password to unlock your electronic device (or ask you to unlock it directly), you can refuse. You may suffer adverse consequences at the hands of law enforcement—from having your phone seized to being booked into custody—for refusing to provide your password or biometric key . Every arrest situation is different, however, and you will need to consider your own threat model .
After the Protest anchor link
What to do if your device is confiscated anchor link

If your device has been confiscated, you may have legal recourse to get it back. In the U.S., your attorney can file a motion for the return of your property if it is not being held as evidence in a pending case. If the police believe that evidence of a crime was found on your electronic device, including in your photos or videos, then the police can keep it as evidence. They may also attempt to end your ownership of your electronic device, but you can challenge such asset forfeiture in court.

You can also revoke access for some services that are logged in on your device. For instance, on Twitter if you go to Settings and privacy -> Apps and devices, you can revoke access for devices that have permission to connect to your Twitter account. For other services, simply changing your password or passphrase will prompt the app to log out. But beware that revoking law enforcement access may expose you to the risk of being charged with obstruction of justice or the destruction of evidence. You should always speak to your attorney first before deciding how to proceed. Online services may provide logs of recent log-ins for your account. If you are worried your device is being used to access accounts without your consent, it might be useful for you to see if such logs are available and monitor them. If law enforcement confiscates your device, they may use a “forensic” tool such as “Cellebrite” to try to extract data from your device, such as images, contacts, messages, and location history. This is more likely to be successful if your phone is older or unencrypted. For this reason, it’s important to carry the bare minimum of data with you, and use the strongest level of encryption, when going into a risky situation like a protest.
