# Basics
[[../module-categories/basics.md|<Grundlagen]]
Wenn du dir noch nie Gedanken gemacht hast um den Schutz deiner digitalen 
[[../Glossar/daten.md|Daten]] oder von Gesprächen mit anderen, dann sind wir hier, um zu helfen.
Surveillance Self-Defense ist ein Handbuch, das dir helfen soll, das [[../Glossar/Risikoabschätzung.md|Risiko]]
abzuschätzen, dass Onlinespionage für dich darstellt. Es kann dir helfen, 
dich vor Überwachung zu schützen, sei es vor Kleinkriminellen oder 
Nationalstaaten. Wir bieten Leitfäden zu den besten Privatssphäre schützenden 
Werkzeugen und erklären, wie Du den Schutz vor Überwachung in deinen Alltag 
integrieren kannst.
Wenn du bereit bist, hilft dir unsere Serie grundlegender Anleitungen (unten), 
zu verstehen, was digitale Überwachung ist und wie du dich verteidigen kannst. 
Wir empfehlen, mit der Anleitung dazu anzufangen, [[../Module/Risikoabschaetzung|dein Risiko abzuschätzen]].
[[../Module/warum-metadaten-wichtig-sind|Warum Metadaten wichtig sind]]
[[../Module/animierte-uebersicht-wie-du-ein-supersicheres-passwort-mit-wuerfeln-erstellst|Animierte Übersicht: Wie du ein supersicheres Passort mithilfe von Würfeln erstellst]]
[[../Module/animierte-uebersicht-schuetze-dein-geraet-vor-hackern|Animierte Übersicht: Schütze dein Gerät vor Hackern]]
[[../Module/animierte-uebersicht-nutze-einen-passwortmanager-um-online-sicher-zu-bleiben|Animierte Übersicht: Nutze einen Passwortmanager, um online sicher zu bleiben]]
[[../Module/animierte-uebersicht-wie-starke-verschlüsselung-helfen-kann-onlineueberwachung-zu-entgehen|Animierte Übersicht: Wie starke Verschlüsselung helfen kann, Onlineüberwachung zu entgehen]]
[[../Module/zur-digitalen-sicherheit-in-sieben-schritten|Zur digitalen Sicherheit in sieben Schritten]]
[[../Module/waehle-deine-werkzeuge|Wähle deine Werkzeuge]]
[[../Module/kommunikation-mit-anderen|Kommunikation mit anderen]]
[[../Module/dein-sicherheits-plan|Dein Sicherheitsplan]]
[[../Module/was-du-ueber-verschluesselung-wissen-solltest|Was du über Verschlüsselung wissen solltest]]
[[../Module/sichere-deine-daten|Sichere deine Daten]]
[[../Module/erstelle-sichere-passwoerter|Erstelle sichere Passwörter]]
