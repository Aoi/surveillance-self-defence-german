# Kommunikation mit Anderen
Telekommunikations-Natzwerke und das Internet machen es einfacher, mit Anderen 
zu kommunizieren als je zuvor, haben aber auch Überwachung mehr verbreitet.
Ohne weitere Schritte zu unternehmen um deine Privatsphäre zu schützen kann
potentiell alle Telefonate, Chatnachrichten, Video- und Audioübertragungen und 
Social Media Posts abgehorcht werden.

Oft ist der für die Privatssphäre schützendste Weg zu kommunizieren sich 
persönlich zu treffen, ohne Computer oder Telefone dabei in irgendeiner Weise
zu involvieren. Weil das nicht immer möglich ist, ist der nächstbeste Schritt
[[../Glossar/e2ee|e2ee]].

# Wie funktioniert e2ee?
e2ee stellt sicher, dass Informationen beim versenden (dem ersten "Ende") in 
eine geheime Nachricht verwandelt werden, die erst bei der:m endgültigen 
Empfänger:in (dem zweiten "Ende") entschlüsselt werden können. Dadurch kann 
niemand mitlesen, was ihr schreibt, auch nicht aus dem gleichen WLAN, als
Netzbetreiber:in und noch nicht mal als Betreiber:in der Website oder App. Es
mag etwas unintuitiv sein, aber nur weil du deine Nachrichten in einer App auf 
deinem Telefon oder Informationen von einer Website abrufen kannst, heißt das 
nicht zwingend, dass die Betreibenden der App oder der Website das auch 
können. Das ist eine Kerneigenschaft von guter [[../Glossar/verschluesselung|Verschlüsselung]]: selbst die, die 
sie entwickeln und einsetzen, sie nicht brechen können.

Alle Werkzeuge, zu denen es auf der SSD Webseite Anleitungen gibt, nutzen e2ee.
Du kannst e2ee für jede Art der Kommunikation benutzen, auch Audio- und 
Videoanrufe, Textnachrichten und Email.

<<< [lightbulb-box]
Nicht zu verwechseln sind e2ee und [[../Glossar/TLS|Transport-Layer Verschlüsselung]].
Während etwa e2ee deine Nachrichten den kompletten Weg bis zur Empfänger:in 
schützt, schützt Transport-Layer Verschlüsselung sie nur auf dem Weg zum Server
der App und vom Server zum Gerät der Empfänger:in. In der Mitte kann dein:e 
Chatanbieter:in - oder die Webseite, die du besuchst oder die App, die du 
nutzt - unverschlüsselte Kopien deiner Nachrichten lesen.
>>>

Im Kern funktioniert e2ee so: Wenn zwei Menschen miteinander ende-zu-ende 
verschlüsselt kommunizieren wollen (nennen wir sie Akiko und Boris), müssen sie
jewils [[../Glossar/Daten|Daten]]schnipsel erzeugen, sogenannte Schlüssel. Diese Schlüssel können 
genutzt werden, um Daten, die alle lesen können, umzuwandeln in Daten, die nur
von jemandem mit dem passenden [[../Glossar/Schlüssel|Schlüssel]] gelesen werden können.
Bevor Akiko eine Nachricht zu Boris schickt, verschlüsselt sie die Nachricht
für Boris Schlüssel, sodass nur Boris sie [[../Glossar/entschlüsseln|entschlüsseln]] kann. Dann schickt sie
diese verschlüsselte Nachricht über das Internet. Falls irgendjemand Akiko und
Boris belauscht, kann nur die verschlüsselte Nachricht gesehen werden, aber 
nicht der Inhalt der Nachricht gelesen werden - selbst, wenn die 
Angreifenden Zugang zum Dienst haben, den Akiko nutzt nutzt, um die 
Nachricht zu versenden (z.B. ihren Email-Account). Wenn Boris die Nachricht 
empfängt, muss er seinen Schlüssel nutzen, um die Nachricht zu entschlüsseln.

<<< [exclamation-mark-box]
Manche Dienste, z.B. Google Hangouts, werben mit "Verschlüsselung", benutzen
aber Schlüssel, die Google erstellt und kontrolliert werden, nicht der 
Person, die die Nachricht am Ende lesen soll. Das ist keine e2ee. Um sicher zu
kommunizieren, sollten nur die "Enden" der Unterhaltung die Schlüssel haben,
mit denen ihr die Nachrichten ver- und entschlüsseln könnt. Wenn der Dienst,
den ihr nutzt, die Schlüssel kontrolliert, ist es stattdessen Transport-Layer
Verschlüsselung.
>>>

e2ee bedeutet, dass die Schlüssel geheim bleiben müssen. Es kann auch bedeuten,
sicherzustellen, dass der Schlüssel, der benutzt wird, von den [[Anleitung-Signal-ios#3|richtigen Leuten]]
[[Anleitung-Whatsapp-Android#2|kontrolliert wird]]. e2ee zu benutzen, kann etwas Aufwand bedeuten - angefangen 
bei der Wahl, eine App zu installieren, die proaktiv die Schlüssel verifiziert -
aber es ist der beste Weg, dem Inhalt der Nachricht so sehr vertrauen zu können
wie der Person, von der sie stammt, ohne der Platform vertrauen zu müssen, die
genutzt wird.

Mehr über Verschlüsselung findest du in [[was-sollte-ich-über-Verschlüsselung-wissen|Was sollte ich über Verschlüsselung wissen?]],
 [[kernkonzepte-Verschluesselung|Kernkonzepte der Verschlüsselung]] und [[verschluesselung-arten|Verschiedene Arten von Verschlüsselung]]. Außerdem erklären wir eine bestimmte Art der Verschlüsselung, sogenannte "[[../Glossar/verschluesselung-assymetrisch|assymetrische Verschlüsselung]]", genauer in 
 [[Einfuehrung-assymetrische-verschluesselung-pgp|Eine Einführung in e2ee]]
 
# Telefonanrufe und Textnachrichten im Gegensatz zu verschlüsselten Nachrichten über das Internet
Wenn du mit deinem Festnetzanschluss oder deinem Mobiltelefon telefonierst, ist
dein Telefonat nicht e2ee. Wenn du eine SMS verschickst, ist der Text in 
garkeiner Weise verschlüsselt. Beides erlaubt Regierungen, und anderen mit 
Einfluss auf die Netzbetreiber:in, deine Nachrichten zu lesen und die Anrufe
aufzunehmen. Wenn deine [[Risikoanalyse]] ein Abhören durch deine Regierung
beinhaltet, kann es für dich sinnvoller sein, Alternativen zu nutzen, die über
das Internet funktionieren. Zusätzlich bieten viele dieser Alternativen auch
Video.

Ein paar Beispiele von Diensten und Software, die e2ee Textnachrichten und 
Audio- und Videoanrufe erlauben sind:
- Signal (für [[Anleitung-Signal-ios|iOS]] und [[Anleitung-Signal-android|Android]])
- WhatsApp (für [[Anleitung-whatsapp-ios|iOS]] und [[Anleitung-whatsapp-android|Android]])
- [[https://wire.com|Wire]]

Ein paar Beispiele voin Diensten, die standardmäßig keine e2ee anbieten, sind:
- Google Hangouts
- Kakao Talk
- Line
- Snapchat
- WeChat
- QQ
- Yahoo Messenger

Manche Dienste, z.B. Facebook Messenger und Telegrad, bieten e2ee nur, wenn du
sie gezielt einschaltest. Wieder andere Dienste, wie z.B. iMessage, bieten e2ee
nur, wenn an beiden Enden ein bestimmtes Gerät genutzt wird (im Fall von 
iMessage müssen alle Beteiligten ein iPhone verwenden).

# Wieweit kannst du deinem Chatdienst vertrauen?

e2ee kann dich schützen vor der Überwachung durch Staaten, Angreifer:innen und 
vor den Chatdiensten selbst. Alle diese Gruppen können aber auch heimliche
Änderungen an der verwendeten Software vornehmen, sodass trotz des Versprechens
e2ee zu nutzen, deine [[../Glossar/Daten|Daten]] unverschlüsselt oder mit 
[[../Glossar/Verschlüsselung|geschwächter Verschlüsselung]] verschickt.

Viele Gruppen, auch die EFF, verbringen viel Zeit damit, weit verbreitete 
Dienste (z.B. WhatsApp, das Facebook gehört, oder Signal) zu beobachten, um
sicherzustellen, dass die e2ee, die versprochen wird, auch tatsächlich 
stattfindet. Wenn du dir wegen solcher Möglichkeiten besorgt bist, kannst du
Werkzeuge nutzen, die öffentlich bekannte und überprüfte 
Verschlüsselungstechniken nutzen. [[../Glossar/OTR|OTR]] und [[../Glossar/PGP|PGP]] sind zwei Beispiele. Diese Systeme
verlassen sich für den sicheren Einsatz auf die Expertise der Nutzenden, sind
häufig weniger nutzendenfreundlich und nutzen ältere Protokolle, die nicht alle
neuesten Verschlüsselungstechniken nutzen.

[[../Glossar/record|Off-the-record]] (OTR) ist ein e2ee [[../Glossar/Protokoll|Protokoll]] für Sofort-Text-Nachrichten, das
mit verschiedenen Diensten genutzt werden kann. Hier ein paar Werkzeuge, die
OTR beinhalten:
- [[Anleitung-otr-linux|Pidgin]] (unter Windows und Linux)
- [[Anleitung-otr-mac]] (unter MacOS)
- Jitsi (unter [[../Handbücher/jitsi/linux|Linux]], [[../Handbücher/jitsi/windows|Windows]], [[../Handbücher/jitsi/os-x|MacOS]])

PGP (kurz für [[../Glossar/pgp|Pretty Good Privacy]], übersetzt aus dem Englischen soviel wie 
"ziemlich gute Privatsphäre") ist ein Standard zur e2ee für Email. Hier findest
Du genaue Anleitungen, wie Du PGP-Verschlüsselung für deine Emails aufsetzen
und nutzen kannst:
- [[Anleitung-pgp-mac-os|Anleitung: PGP unter macOS]]
- [[Anleitung-pgp-windows|Anleitung: PGP unter Windows]]
- [[Anleitung-pgp-linux|Anleitung: PGP unter Linux]]

<<< [lightbulb-box]
PGP für Emails ist am besten geeignet für technisch erfahrene Nutzende, die mit
anderen technisch erfahrenen Nutzenden kommunizieren, die sich alle im Klaren
sind über PGPs Komplexität und Grenzen.
>>>

# Was e2ee nicht leistet

e2ee schützt nur den Inhalt der Unterhaltung, lässt aber sichtbar, dass sie
überhaupt statt findet. Es schützt auch nicht die [[../Glossar/Metadaten|Metadaten]], also z.B. den 
Betreff einer Email, mit wem Du kommunizierst und wann. Wenn du von einem
Mobiltelefon aus telefonierst, gehört auch dein Standort zu diesen Metadaten.

<<< [exclamation-mark-box]
Auch, wenn der konkrete Inhalt der Unterhaltung geheim bleibt, können
Metadaten alleine schon sehr aufschlussreich sein.
>>>

Metadaten deiner Anrufe können sehr intime und sensible Informationen 
offenbaren. Zum Beispiel:
- Sie offenbaren, dass Du eine Telefon-Sex Hotline um 2:24 Uhr in der Nacht 
  angerufen hast und 18 Minuten lang telefoniert hast, aber es ist nicht 
  bekannt, worum es dabei ging.
- Sie offenbaren, dass Du von der Golden-Gate-Bridge aus eine Telefonseelsorge 
  angerufen hast, aber worum es ging bleibt geheim.
- Sie offenbaren, dass du innerhalb einer Stunde mit einem HIV-Test-Service 
  gesprochen hast, dann deiner:m Ärtzt:in und dann deiner Krankenversicherung,
  aber nicht, worüber gesprochen wurde.
- Sie offenbaren, dass du einen Anruf gekriegt hast von deinem lokalen NRA-Büro 
  und, dass du anschließend sofort diejenigen angerufen hast, die dich 
  demokratisch repräsentieren, aber nicht worüber gesprochen wurde.
- Sie offenbaren, dass du erst mit deiner:m Gynokolog:in für 1,5 Stunden 
  gesprochen hast und später am Tag bei einer Beratungsstelle für Abtreibungen
  angerufen hast, aber worüber ihr gesprochen habt, erfährt niemand.
  
# Andere wichtige Features
# TODO ab hier links einfügen

[[../Glossar/e2ee|e2ee]] ist nur eines von vielen Featuren, die [[https://www.eff.org/deeplinks/2018/03/why-we-cant-give-you-recommendation|für dich wichtig]] sein können, um
[[https://www.eff.org/deeplinks/2018/03/secure-messaging-more-secure-mess|sicher zu kommunizieren]]. Wie oben beschrieben, ist e2ee großartig, um 
Unternehmen und Regierungen daran zu hindern, deine Nachrichten zu lesen.
Aber für viele Menschen sind Unternehmen und Regierungen nicht die größte 
[[../Glossar/Bedrohung|Bedrohung]], weshalb e2ee nicht die allererste Priorität sein muss.

Etwa, wenn sich jemand Sorgen macht, [[https://www.eff.org/deeplinks/2018/03/thinking-about-what-you-need-secure-messenger#ephemeral |weil ein:e Partner:in, Elternteil oder Arbeitgeber]]
physisch auf das Telefon zugreifen können; dann kann die Möglichkeit 
selbst-zerstörende, "verschwindende" Nachrichten zu senden entscheidend sein 
bei der [[https://www.eff.org/deeplinks/2018/03/thinking-about-what-you-need-secure-messenger|Wahl des Nachrichtendiensts]]. Jemand anderes könnte ein Problem damit 
haben, [[https://www.eff.org/deeplinks/2018/03/thinking-about-what-you-need-secure-messenger#alias|eine Telefonnummer weiter zu geben]], kann es wichtig sein, dass ein 
anderes "Alias" als eine Telefonnummer verwendet werden kann.

Ganz allgemein sind [[https://www.eff.org/deeplinks/2018/03/building-secure-messenger|Sicherheits- und Privatsphärefeatures]] nicht das einzige
Kriterium, das eine Rolle spielt bei der Wahl eines sicheren 
Kommunikationsmittels. Eine App mit besseren Sicherheitsfeatures ist wertlos,
[[https://www.eff.org/deeplinks/2017/01/data-privacy-day-play-privacy-team-sport|wenn niemand sie nutzt, mit denen Du kommunizieren willst]], und zwischen 
verschiedenen Ländern oder Gemeinschaften kann sich die Beliebtheit der 
verschiedenen Apps stark unterscheiden. Auch schlechte Zugänglichkeit oder für 
eine App bezahlen zu müssen kann einen Nachrichtendienst für manche Menschen 
unbrauchbar machen.

Je klarer Du verstehst, wozu du einen sicheren Kommunikationsweg brauchst, 
umso leichter wird es, dich zurecht zu finden in der Fülle an umfangreichen, 
widersprüchlichen und teils veralteten Informationen, die verfügbar sind.
