# Animierte Übersicht: Wie starke Verschlüsselung helfen kann, Onlineüberwachung zu entgehen
Es ist nicht zu lange her, dass Spionage eine Zeitintensive Arbeit war, die
übermäßig Resourcen und konkrete Ziele erforderte. Heutzutage ist es sehr viel 
einfacher geworden für Regierungen und Gegner, Einzelpersonen auszuspionieren...
oder übrigens auch alles und jeden. Im 21. Jahrhundert zu leben, bedeutet, dass
du in dem Moment, in dem du das Internet benutzt, um etwas zu speichern, zu 
bearbeiten oder zu teilen, anfällig wirst für Überwachung.

Mit einem Großteil des Datenverkehrs im Internet sichtbar für Jede:n mit 
Zugriff auf einen der Computer, die die Nachricht durchlaufen - von deinem 
Computer zu Hause, über deinen lokalen Internetanbieter, bis hin zu dem 
Rechner, der die Nachricht empfängt und alle Anderen, die noch dazwischen 
sitzen - wie soll da irgendwer in der Lage sein, ihre Unterhaltung vertraulich
zu halten?

Die Antwort liegt in der Nutzung starker [[../Glossar/verschluesselung|Verschlüsselung]]. Verschlüsselte 
Nachrichten werden in eine Form gebracht, die von niemandem gelesen werden 
kann, außer der Person am endgültigen Zielort. Wir sehen mehr und mehr 
Webseiten, die Verschlüsselung nutzen, um [[../Glossar/daten|Daten]] im Transit zu schützen, aber
leider ist es für viele Internetdienste immer noch nicht Standard. Um selbst
mehr Gebrauch von Verschlüsselung zu machen und deine Daten zu schützen, kannst
du [[https://www.eff.org/https-everywhere|HTTPS Everywhere]] (engl. "HTTPS überall) installieren, ein Browser add-on,
das Webseiten auffordert, verschlüsselt zu kommunizieren, was Spione zurück 
ins Zeitalter der gezielten Überwachung wirft.

