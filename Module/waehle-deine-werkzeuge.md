# Die Werkzeugauswahl
Bei einer so großen Auswahl an Tools von Firmen und Webseiten, die alle 
Einzelpersonen helfen sollen, ihre digitale Sicherheit zu verbessern, wie wählt
man da die richtigen Werkzeuge aus?

Wir haben keine todsichere Liste von Werkzeugen, mit denen du dich verteidigen
kannst (auch wenn du Teile einer üblichen Auswahl in unseren [[../Kategorien/werkzeuganleitungen|Werkzeuganleitungen]]
findest). Aber wenn du ein klares Bild davon hast, was du beschützt, und vor 
wem, dann kann dir diese Anleitung einen grundlegenden Anhaltspunkt geben bei
der Werkzeugwahl.

<<< [lightbulb-box]
Denk daran, Sicherheit kommt nicht durch deine Werkzeuge oder die Software, die
du herunterlädst. Sie fängt damit an, dass du die spezifischen Bedrohungen 
kennst, denen du gegenüber stehst, und wie du ihnen begegnen kannst. Probier
unsere Anleitung zur [[../Module/Risikobeurteilung|Risikobeurteilung]] für mehr Information.
>>>

# Sicherheit wird gemacht, nicht gekauft
Bevor du die Software, die du nutzt, wechselst oder neue Werkzeuge kaufst, ist
das wichtigste, dir im klaren zu sein, dass kein einzelnes Stück Software 
absolute Sicherheit schafft. Es ist daher wichtig, deine digitalen 
Sicherheitsmaßnahmen in ihrem Kontext verstehst. Wenn du zum Beispiel bei 
deinem Telefon sichere Werkzeuge verwendest, aber dein Computer nicht mit einem 
[[../Glossar/Passwort|Passwort]] geschützt ist, kann dich unter Umständen jeder noch so sichere Umgang 
mit dem Telefon nicht schützen. Wer Informationen über dich sammeln will, wird 
den leichtesten Weg gehen, nicht den schwersten.

Zweitens ist es nicht möglich, sich gegen jede Angriffsmethode und jeden Trick
oder Angreifer:in zu schützen. Du solltest dich daher darauf konzentrieren, wer
ein Interesse daran hat, deine [[../Glossar/Daten|Daten]] zu sammeln, was sie damit machen wollen,
und welche Möglichkeiten sie haben, daran zu kommen. Wenn die größte [[../Glossar/Bedrohung|Bedrohung]]
für dich physische Überwachung durch eine:n Privatdetektiv:in ist, die keinen 
Zugang zu online Überwachungswerkzeugen haben, wirst du kein Cryptophone 
brauchen, das behauptet "NSA-sicher" zu sein. Wenn dein Problem aber eine 
Regierung ist, die Gegner:innen regelmäßig allein für den Gebrauch
von [[../Glossar/Verschlüsselung|Verschlüsselungstechnik]] wegsperrt, kann es sinnvoller sein, unauffälligere 
Taktiken zu nutzen. Das kann etwa bedeuten, dass ihr eine Reihe harmlos
klingender Codes festlegt, mit denen ihr sagen könnt, was ihr zu sagen habt,
ohne das [[../Glossar/Risikoabschaetzung|Risiko]], dass auf euren Rechner Spuren von der Verschlüsselungssoftware
bleiben. Eine Liste möglicher Angriffe zu erstellen und dir Pläne zu machen,
wie du dich dagegen wehren kannst, wird [[../Module/dein-sicherheits-plan|threat modelling]] genannt.

Das alles gesagt, sind hier ein paar Fragen, die du dir stellen kannst, bevor
du ein Werkzeug runterlädst, kaufst oder nutzt.

# Wie Transparent ist es?
Sicherheitsforscher:innen gehen stark davon aus, dass Offenheit und Transparenz
zu sichereren Werkzeugen führt.

Viele der Community rund um digitale Sicherheit nutzen und empfehlen Software,
die Open Source ist. Das kommt aus dem Englischen und bedeutet, dass der 
Quellcode, der beschreibt, wie das Programm funktioniert, öffentlich verfügbar
ist und dass Anderen erlaubt und möglich ist, damit das Programm zu untersuchen,
anzupassen und zu teilen. Durch diese Transparenz laden die Schreiber:innen
dieser Werkzeuge andere dazu ein, Sicherheitsprobleme zu finden und dabei zu
helfen, das Programm zu verbessern.

<<< [exclamationmark-box]
[[../Glossar/open-source-software|Open-Source-Software]] bietet eine Möglichkeit für bessere Sicherheit, garantiert
sie aber nicht. Der Vorteil von open-source Software ist zum Teil angewiesen 
auf eine Gemeinschaft von Techniker:innen, die sich tatsächlich den Code 
ansehen, was für kleine Projekte, aber auch für große, komplexe Projekte, eine
Herausforderung darstellen kann.
>>>

Wenn du überlegst, ein Werkzeug einzusetzen, sieh nach, ob sein Quellcode 
verfügbar ist und ob es einen Sicherheitsaudit von unabhängigen 
Sicherheitsforscher:innen hat, der bescheinigt, wie sicher das Programm ist.
In jedem Fall sollte Software und Hardware eine ausführliche technische 
Erklärung haben, wie sie funktionieren, um anderen Expert:innen beim 
untersuchen zu helfen.

# Wie anschaulich sind die Schreiber:innen über Vor und Nachteile?
Software oder Hardware, die völlig sicher ist, gibt es nicht. Such nach den 
Werkzeugen mit Schreiber:innen oder Verkäufer:innen, die ehrlich und offen über
die Schwächen des Produkts sprechen.

<<< [exclamationmark-box]
Pauschale Anpreisungen des Codes als "militärische Güteklasse" oder "NSA-
sicher" sind große Warnsignale. Diese Formulierung deutet darauf hin, dass
die Schreiber:innen unerhältnismäßiges Vertrauen haben oder nicht wahrhaben 
wollen, dass ihr Produkt Mängel haben kann.
>>>

Angreifer:innen suchen nach immer neuen Wegen, die Sicherheit von Programmen
zu überwinden. Deswegen müssen Software und Hardware aktualisiert werden, um
bekannte Schwachstellen zu beheben. Das kann ein echtes Problem sein, wenn die
Schreiber:innen das nicht tun wollen, sei es aus Angst vor negativer 
Aufmerksamkeit, oder weil sie die dafür notwendige Infrastruktur nicht 
vorbereitet haben. Suche nach Schreiber:innen, die bereit sind, diese 
Aktualisierungen zu machen und die offen und ehrlich darüber sprechen, warum
sie das tun.

Ein guter Anhaltspunkt für künftiges Verhalten ist hierbei, wie in der 
Vergangenheit mit ähnlichen Problemen umgegangen wurde. Wenn die Website 
Probleme älterer Versionen listet und auf regelmäßigen Aktualisierungen und
Informationen aufmerksam macht - etwa wie lang die letzte Aktualisierung genau
her ist - kannst du zuversichtlicher sein, dass sie diese Möglichkeit auch in 
Zukunft anbieten.

# Was passiert, wenn die Sicherheit der Schreiber:innen beeinträchtigt wird?
Wenn Schreiber:innen von Sicherheitswerkzeugen software und hardware schreiben,
müssen sie (ganz wie auch du) ein klare Bild ihrer Bedrohungen haben. Die 
besten Hersteller:innen beschreiben explizit in ihrer Dokumentation, vor 
welchen Gefahren das Werkzeug dich schützen kann.

Über einen möglichen Angriff wollen viele Hersteller:innen aber nicht sprechen:
Sich selbst. Was, wenn sie selbst kompromittiert werden oder sich entschließen,
ihre Nutzer:innen [[../Glossar/Angriff|anzugreifen]]? Ein Gericht oder eine Regierung könnte eine 
Firma etwa davon überzeugen, persönliche [[../Glossar/daten|Daten]] zu übergeben oder eine 
"Hintertür" einzubauen, die all die Schutzfunktionen aushebelt, die das Werkzeug
eigentlich bereitstellen soll. Achte daher auch darauf, an was für Gesetze die 
Schreiber:innen sich halten müssen. Wenn du dich etwa vor der Iranischen 
Regierung schützen möchtest, könnte eine Firma in den USA sich iranischen 
Urteilen widersetzen, selbst wenn sie sich an amerikanische Anordnungen halten
muss.
Aber auch wenn Schreiber:innen Regierungsdruck standhalten können, könnten 
Angreifer:innen versuchen, in die Systeme der Macher:innen eindringen um die
Nutzer:innen anzugreifen.
Die widerstandsfähigsten Werkzeuge sind solche, die das als möglichen Angriff
ernst nehmen und darauf ausgelegt sind, sich dagegen zu verteidigen. Suche 
lieber nach Versicherungen, dass die Macher:innen auf private Daten nicht 
zugreifen *können*, als Versprechen, dass sie es nicht *werden*. Schau nach 
Organisationen, die [[https://www.eff.org/who-has-your-back-2017|einen Ruf haben, sich gegen gerichtliche Anordnungen zur Herrausgabe von persönlichen Daten zu wehren]].

# Wurde es zurückgerufen oder online kritisiert?
Unternehmen, die ihre Produkte verkaufen, und Enthusiast:innen, die ihre 
neueste Software bewerben, können irregeführt werden, irreführend sein und 
auch schlicht lügen. Ein Produkt, das ursprünglich sicher war, kann in Zukunft
schwerwiegende Fehler haben. Stell sicher, dass du aktuelle Nachrichten zu 
deinen Werkzeugen kriegst.

<<< [lightbulb-box]
Es ist viel Arbeit für eine einzelne Person, auf dem aktuellen Stand zu einem
Werkzeug zu bleiben. Wenn du Kolleg:innen hast, die ein bestimmtes Produkt oder
einen Dienst nutzen, haltet euch gegenseitig auf dem Laufenden.
>>>

# Was für ein Telefon sollte ich kaufen? Welchen Computer?
Sicherheits-Trainer:innen werden oft gefragt: "Sollte ich ein Android oder ein
iPhone kaufen?" oder "Sollte ich einen PC oder einen Mac nutzen?" oder "Welches
[[../Glossar/Betriebssystem|Betriebssystem]] soll ich nutzen?" Es gibt auf diese Fragen keine einfache 
Antworten. Es werden ständig neue Lücken gefunden und alte geschlossen. 
Unternehmen können miteinander konkurrieren, dir bessere Sicherheit zu bieten,
oder sie stehen unter Druck von Regierungen, diese Sicherheit zu schwächen.

Etwas allgemeiner Rat lässt sich aber immer anwenden. Wenn du ein Gerät oder 
Betriebssystem anschaffst, halte es immer auf dem aktuellen Stand mit 
Softwareaktualisierungen. Updates schließen oft Probleme in älterem Code, die 
für Angriffe ausgenutzt werden kann. Beachte, dass manche ältere Telefone oder 
Betriebssysteme nicht mehr mit Sicherheitsaktualisierungen unterstützt werden.
Zum Beispiel hat Microsoft sehr klar gemacht, dass Windows Vista, XP, 7, 8 und
8.1 auch für schwerwiegende Sicherheitsprobleme keine Aktualisierung mehr 
erfahren. Das bedeutet, dass wenn du sie nutzt, du nicht erwarten kannst,
sicher vor Angriffen zu sein. Das gleiche gilt für OS X das älter ist als 10.14
oder Mojave.
Jetzt, wo du dir im klaren bist über deine Bedrohungen und worauf du an
Sicherheitswerkzeugen achten kannst, kannst du eine bewusstere Entscheidung
treffen, welches die für dich in deiner Situation passenden Werkzeuge sind.

# Produkte, die Überwachungs Selbst-Verteidigung erwähnt
Wir versuchen sicher zu stellen, dass Software und Hardware, die hier erwähnt 
wird die folgenden Kriterien erfüllt. Wir sind guten Glaubens, nur Produkte zu 
listen, die:
* berücksichtigen, was wir zur Zeit über Sicherheit wissen,
* transparent mit ihren Abläufen (und Scheitern) sind,
* Verteidigungen dagegen haben, dass ihre Macher:innen selbst kompromittiert 
  werden,
* aktuell gepflegt werden, mit einer großen und technisch versierten 
  Nutzer:innen-Basis.
Wir gehen davon aus, dass sie zum Zeitpunkt des Schreibens von einem breiten
Publikum beobactet werden, das sie auf Fehler untersucht und diese schnell 
meldet. Bitte habe Verständnis, dass wir nicht die Resourcen haben, sie selbst
genauer zu untersuchen oder unabhängige Überprüfungen ihrer Sicherheit 
anzustellen. Wir befürworten diese Produkte nicht und können ihre komplette 
Sicherheit nicht garantieren.

